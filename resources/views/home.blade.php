@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Productos</div>

                <div class="card-body">

                    <!--For Page-->
                    <div class="page">
                        <!--For Row containing all card-->
                        <div class="row">

                            @foreach ($products as $product)

                                <div class="col-sm">
                                    <div class="card card-cascade card-ecommerce wider shadow mb-5">
                                        <!--Card image-->
                                        <div class="view view-cascade overlay text-center">
                                            <img class="card-img-top" src="{{ URL::to('/') }}/img/producto-{{ $product->id }}.jpg" alt="">
                                            <a>
                                                <div class="mask rgba-white-slight"></div>
                                            </a>
                                        </div>
                                        <!--Card Body-->
                                        <div class="card-body card-body-cascade text-center">
                                            <!--Card Title-->
                                            <h4 class="card-title"><strong><a href="">{{ $product->name }}</a></strong></h4> <!-- Card Description-->
                                            <p class="card-text">{{ $product->description }}</p>
                                            <p class="price">{{ money_format('%.0n', $product->price) }}</p>
                                            <!--Card footer-->

                                            @if(Auth::check())
                                                <div class="card-footer">
                                                    <a class="btn btn-primary btn-lg btn-block" href="{{ route('new-order', $product->id) }}" role="button">Comprar</a>
                                                </div>
                                            @else
                                                <div class="card-footer">
                                                    <a class="btn btn-primary btn-lg btn-block" href="{{ route('register') }}" role="button">Registrase para comprar</a>
                                                </div>
                                            @endif 
                                            </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
