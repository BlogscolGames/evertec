@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="d-flex flex-column justify-content-center align-items-center" id="order-heading">
                <div class="text-uppercase">
                    <p>Order detail</p>
                </div>

                <?php
                    $orden = Session::get('orden');
                    $producto = Session::get('producto');
                ?>

                <div class="h4">{{date('l, j F Y', strtotime($orden['created_at']))}}</div>
                <div class="pt-1">
                    <p>Order #{{$orden['id']}} is currently<b class="text-dark"> processing</b></p>
                </div>
            </div>

            <div class="wrapper bg-white">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <thead>
                            <tr class="text-uppercase text-muted">
                                <th scope="col">product</th>
                                <th scope="col" class="text-right">total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">{{$producto['name']}}</th>
                                <td class="text-right"><b>{{money_format('%.0n', $producto['price']) }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="d-flex justify-content-start align-items-center list py-1">
                    <div><b>1</b></div>
                    <div class="mx-3"> <img src="{{ URL::to('/') }}/img/producto-{{ $producto['id']}}.jpg" alt="{{$producto['name']}}" class="rounded-circle" width="30" height="30"> </div>
                    <div class="order-item">{{$producto['name']}}</div>
                </div>

                <div class="pt-2 border-bottom mb-3"></div>
                    <div class="d-flex justify-content-start align-items-center pl-3 py-3 mb-4 border-bottom">
                    <div class="text-muted"> Today's Total </div>
                    <div class="ml-auto h5">{{money_format('%.0n', $producto['price']) }}</div>
                </div>

                <div class="d-flex justify-content-start align-items-center pl-3 py-3 mb-4 border-bottom">
                    <a class="btn btn-primary btn-lg btn-block" href="{{ URL::to('/pay-order') }}/{{$orden['id']}}" role="button">Pagar</a>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection