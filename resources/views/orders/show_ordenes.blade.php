@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="d-flex flex-column justify-content-center align-items-center" id="order-heading">
                <div class="text-uppercase">
                    <p>Listado de ordenes</p>
                </div>
            </div>
            
            @foreach ($ordenes as $orden)
                <div class="wrapper bg-white">
                    <div class="d-flex justify-content-start align-items-center list py-1">
                        <div><b>Orden #{{ $orden['id']}}</b></div>
                        <div class="mx-3"> <img src="{{ URL::to('/') }}/img/producto-{{ $orden['productID']}}.jpg" alt="{{$orden['productoName']}}" class="rounded-circle" width="30" height="30"> </div>
                        <div class="order-item">
                            <a href="{{ URL::to('/status-order/' . $orden['id']) }}">{{$orden['productoName']}}</a>
                        </div>
                    </div>

                    <div class="pt-2 border-bottom mb-3"></div>
                        <div class="d-flex justify-content-start align-items-center pl-3 py-3 mb-4 border-bottom">
                        <div class="text-muted"> Total </div>
                        <div class="ml-auto h5">{{money_format('%.0n', $orden['productPrice']) }}</div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection