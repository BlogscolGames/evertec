<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
        	['name' => 'Mobile', 'description' => 'This is a Mobile phone with all the advance features and at best price.', 'price' => '500000'],
        	['name' => 'Camera', 'description' => 'Comes with advance sensors, this is a perfect match for photographers.', 'price' => '300000'],
        	['name' => 'Watch', 'description' => 'Smart watch with Easy-to-go connect option and fitness tracking functions.', 'price' => '200000']
        ]);
    }
}
