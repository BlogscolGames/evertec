<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index', function () {
    return view('home');
});


Route::get('/show-ordenes','OrdersController@showOrdenes')->middleware('auth')->name('show-ordenes');

Route::resource('user','UserController')->middleware('auth');

Auth::routes();


Route::get('/new-order/{idProducto}','OrdersController@newOrder')->middleware('auth')->name('new-order');
Route::get('/order-resume/{idOrder}','OrdersController@resumeOrder')->middleware('auth');

Route::post('/save-order/','OrdersController@storeOrder')->middleware('auth')->name('save-order');

Route::get('/pay-order/{idOrder}','PlacetoPayController@realizarPago')->middleware('auth');

Route::get('/status-order/{idOrder}','PlacetoPayController@showStatus')->middleware('auth')->name('status-order');