<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Products extends Model
{
    protected $fillable = [
        'name', 'description', 'price'
    ];

    public static function getProducts($cant)
    {
        $products = DB::table('products')->paginate($cant);
        return $products;
    }
}