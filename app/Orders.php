<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $fillable = [
        'customer_name', 'customer_email', 'customer_mobile', 'status', 'productID', 'productPrice', 'requestId', 'processUrl'
    ];
}