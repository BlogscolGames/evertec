<?php

namespace App\Http\Controllers;

use Config;

use App\Orders;
use App\Products;
use URL;

use Illuminate\Http\Request;
use Dnetix\Redirection\PlacetoPay;

class PlacetoPayController extends Controller
{
	public function placetopay()
	{
	    $datos = new PlacetoPay([
	        'login' => getenv('P2P_LOGIN'),
	        'tranKey' => getenv('P2P_TRANKEY'),
	        'url' => getenv('P2P_URL'),
	        'type' => getenv('P2P_TYPE') ?: PlacetoPay::TP_REST,
	    ]);
	    return $datos;
	}

	public function showStatus($idOrder)
	{
        $orden = Orders::find($idOrder);

        if($orden == null)
			return redirect('/');

        $producto = Products::find($orden->productID);


		try
		{
			$requestId = $orden->requestId;

		    $response = $this->placetopay()->query($requestId);

		    if ($response->isSuccessful())
		    {
		    	$orden->status = $response->status()->status();
		    	$orden->save();
				$message = $response->status()->message();

		        if ($response->status()->isApproved())
		        {
		        }
		        else
		        {
		        }
		    }
		    else
		    {
		        print_r($response->status()->message() . "\n");
		    }
		} catch (Exception $e)
		{
        	$message = "";
		    var_dump($e->getMessage());
		}

		return view('orders.status_order', compact('orden','producto','message'));
	}

	public function realizarPago($idOrder)
	{
        $orden = Orders::find($idOrder);
        $producto = Products::find($orden->productID);

		$reference = 'TEST_' . time();

		// Request Information
		$request = json_decode('{
		    "locale": "es_CO",
		    "buyer": {
		        "name": "' . $orden->customer_name . '",
		        "surname": "",
		        "email": "' . $orden->customer_email . '",
		        "mobile": ' . $orden->customer_mobile . '
		    },
		    "payment": {
		        "reference": "' . $orden->id . '",
		        "description": "Pago en PlacetoPay",
		        "amount": {
		            "details": [
		                {
		                    "kind": "subtotal",
		                    "amount": "' . $orden->productPrice . '"
		                },
		                {
		                    "kind": "discount",
		                    "amount": 0
		                },
		                {
		                    "kind": "shipping",
		                    "amount": "0.0000"
		                }
		            ],
		            "currency": "COP",
		            "total": "' . $orden->productPrice . '"
		        },
		        "shipping": {
		            "name": "' . $orden->customer_name . '",
		            "surname": "",
			        "email": "' . $orden->customer_email . '",
			        "mobile": ' . $orden->customer_mobile . '
		        }
		    },
		    "returnUrl": "'. URL::to('/') . '/status-order/' . $orden->id . '",
		    "expiration": "' . date('c', strtotime('+2 days')) . '",
		    "ipAddress": "127.0.0.1",
		    "userAgent": "Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/55.0.2883.87 Safari\/537.36"
		}', true);

		try
		{
		    $placetopay = $this->placetopay();
		    $response = $placetopay->request($request);

		    if ($response->isSuccessful())
		    {
		    	$orden->requestId = $response->requestId();
		    	$orden->processUrl = $response->processUrl();
   	            $orden->save();

		    	return redirect($response->processUrl());
	    	}
	    	else
	    	{
	    		dd($response->status()->message());
	    	}
		} catch (Exception $e)
		{
	    	dd($e->getMessage());
		}
	}
}
