<?php

namespace App\Http\Controllers;

use Auth;

use App\Orders;
use App\Products;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newOrder(Request $request, $idProducto)
    {
        return view('orders.new_order')->with('idProducto', $idProducto);
    }

    public function resumeOrder(Request $request, $idOrder)
    {
        return view('orders.order_resume')->with('idOrder', $idOrder);
    }

    public function showOrdenes(Request $request)
    {
        $ordenes = Orders::all();
        
        foreach ($ordenes as $i=>$orden)
        {
            $producto = Products::find($orden->productID);
            $ordenes[$i]->productoName = $producto->name;
        }        

        return view('orders.show_ordenes', compact('ordenes'));
    }

    public function storeOrder(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'name' => 'required|string|min:8|max:80',
            'email' => 'required|string|min:4|max:120',
            'celular' => 'required|string|min:10|max:40'
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator)
                        ->withInput();
        }
        else
        {
            $idProducto = $request->get('productoID');

            $producto = Products::find($idProducto);
            $orden = new Orders([
                'customer_name' => $request->get('name'),
                'customer_email'=> $request->get('email'),
                'customer_mobile'=> $request->get('celular'),
                'status'=> 'CREATED',

                'requestId'=> "",
                'processUrl'=> "",

                'productID'=> $idProducto,
                'productPrice'=> $producto->price
            ]);

            $orden->save();

            return redirect('/order-resume/' . $orden->id)->with( ['orden' => $orden, 'producto' => $producto] );
        }
    }

}